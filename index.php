<!doctype html>
<html lang="ru">
<head>
    <title>Свадьба на Кипре - With Love</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="UTF-8">
</head>
<body>
<?php
$Path = __DIR__ . '/sections/';
require $Path . '01_header.php';
require $Path . '02_services.php';
require $Path . '03_kipr.php';
require $Path . '04_places.php';
require $Path . '05_gallery.php';
require $Path . '06_about.php';
require $Path . '07_video.php';
require $Path . '08_reviews.php';
require $Path . '09_contacts.php';
require $Path . '10_order.php';
require $Path . '11_footer.php';
?>

<link rel="stylesheet" href="/index.min.css"/>
<link href='http://fonts.googleapis.com/css?family=Roboto+Slab:400,300&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
<script src="/index.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCnGIPkiVdXlEDy3BPfN4angA8_5t7nWjg&callback=initMap"
        async defer></script>


</body>
</html>
