var Layouts_Grid = (function () {
    function Layouts_Grid() {
        var Object = this;
        $('body').keydown(function (Event) {
            if (Event.keyCode == 81 && Event.altKey) {
                // Alt + Q
                if ($('.JS-Layouts-Grid').length) {
                    Object.Remove_Path();
                }
                else {
                    Object.Create_Path('rgba(0,0,0,0.3)');
                }
            }
            else if (Event.keyCode == 87 && Event.altKey) {
                // Alt + W
                if ($('.JS-Layouts-Grid').length) {
                    Object.Remove_Path();
                }
                else {
                    Object.Create_Path('rgba(255,255,255,0.5)');
                }
            }
        });
    }

    Layouts_Grid.prototype.Create_Path = function ($Color) {
        var Cell_Width = 24, Cell_Height = 8, Width = 1200, Height = $(document).height();
        $('.JS-Layouts-Grid').remove();
        var Canvas = $('<canvas/>', { class: 'JS-Layouts-Grid' })
            .css({
            'display': 'block',
            'top': '0',
            'left': '50%',
            'margin-left': -Width / 2,
            'z-index': '100',
            'position': 'absolute'
        })
            .appendTo('body');
        var Context = Canvas[0].getContext("2d");
        Context.beginPath();
        Canvas[0].width = Width;
        Canvas[0].height = Height;
        Context.strokeStyle = $Color;
        Context.lineWidth = 1;
        // Horizontal
        for (var Index = 0; Index < Height / Cell_Height + 1; Index++) {
            Context.moveTo(0, Index * Cell_Height);
            Context.lineTo(Width, Index * Cell_Height);
        }
        // Vertical
        for (var Index = 0; Index < Width / Cell_Width + 1; Index++) {
            Context.moveTo(Index * Cell_Width, 0);
            Context.lineTo(Index * Cell_Width, Height);
        }
        Context.stroke();
        Context.beginPath();
        Cell_Height = Cell_Height * 5;
        Cell_Width = Cell_Width * 5;
        // Horizontal
        for (var Index = 0; Index < Height / Cell_Height + 1; Index++) {
            Context.moveTo(0, Index * Cell_Height);
            Context.lineTo(Width, Index * Cell_Height);
        }
        // Vertical
        for (var Index = 0; Index < Width / Cell_Width + 1; Index++) {
            Context.moveTo(Index * Cell_Width, 0);
            Context.lineTo(Index * Cell_Width, 3000);
        }
        Context.strokeStyle = $Color;
        Context.lineWidth = 1;
        Context.stroke();
        Context.closePath();
    };
    Layouts_Grid.prototype.Remove_Path = function () {
        $('.JS-Layouts-Grid').remove();
    };
    return Layouts_Grid;
}());
new Layouts_Grid();
