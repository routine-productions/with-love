// Header: To contacts

$('.Header-Call').click(function () {
    $("html, body").animate({scrollTop: $(document).height()}, 400);
});

// Header: Slider
$('.Header-Slider').JS_Carousel({
    Effect: 'fade',
    Navigation_Active: false,
    Range: 1,
    Shift: 1,
    Next: $('<img class="Header-Slider-Next" src="/img/ico/arrow-next.png" alt=""/>'),
    Previous: $('<img class="Header-Slider-Prev" src="/img/ico/arrow-prev.png" alt=""/>')
});

// Header: Heart
setInterval(function () {
    $('.Header-Logo img').addClass('Scale').delay(500).queue(function (next) {
        $(this).removeClass('Scale').dequeue();
    });
}, 1000);

// Header: Scale
function Blocks_Height() {
    $('.Header').css('height', $(window).height());
}
$(window).resize(function () {
    Blocks_Height();
});
Blocks_Height();


// Places: Slider
$('.Places-Slider').JS_Carousel({
    Effect: 'fade',
    Navigation_Active: false,
    Range: 1,
    Shift: 1,
    Next: $('<img class="Places-Slider-Next" src="/img/ico/arrow-next.png" alt=""/>'),
    Previous: $('<img class="Places-Slider-Prev" src="/img/ico/arrow-prev.png" alt=""/>')
});

// Video: Load
$(window).load(function () {
    $('.Video').show();
});

// Reviews: More
$('.Reviews a.More').click(function () {
    $(this).parents('article').find('.Hidden').slideToggle();
    $(this).parents('.Reviews-List-Item').siblings().find('.Hidden').slideUp();
    $(this).parents('.Reviews-List-Item').siblings().find('.More').text("Подробнее");

    $(this).text(
        $(this).text() == "Подробнее" ? "Скрыть" : "Подробнее"
    );
    return false;
});

// Contacts:
var map;
function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: 35.049148, lng: 33.211870},
        zoom: 8
    });
}
