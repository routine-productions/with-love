<section class="Video" style="display: none;" id="video">
    <div class="Video-Wrapper">
        <div class="Video-Header">
            <div class="Header-Spot-1"><span></span></div>
            <h3>Видео со свадеб</h3>
            <div class="Header-Spot-2"><span></span></div>
        </div>

        <ul class="Video-Description">
            <li>
                <iframe src="https://www.youtube.com/embed/StR5-BosnL0" frameborder="0" allowfullscreen></iframe>
            </li>
            <li>
                <iframe src="https://www.youtube.com/embed/smEsgey9yJc" frameborder="0" allowfullscreen></iframe>
            </li>
            <li>
                <iframe src="https://www.youtube.com/embed/xXOTiLohFZA" frameborder="0" allowfullscreen></iframe>
            </li>
            <li>
                <iframe src="https://www.youtube.com/embed/kNB_q246k88" frameborder="0" allowfullscreen></iframe>
            </li>
            <li>
                <iframe src="https://www.youtube.com/embed/p3KPs1ChAQc" frameborder="0" allowfullscreen></iframe>
            </li>
            <li>
                <iframe src="https://www.youtube.com/embed/PK8RoByfQ7E" frameborder="0" allowfullscreen></iframe>
            </li>
        </ul>
    </div>
</section>