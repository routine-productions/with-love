<section class="Services Dark" id="services">
    <div class="Services-Wrapper">
        <div class="Services-Header">
            <div class="Header-Spot-1"><span></span></div>
            <h3>Наши услуги</h3>
            <div class="Header-Spot-2"><span></span></div>
        </div>

        <ul class="Services-List">
            <li class="Services-Item">
                <img class="Services-Image" src="/img/services/service-01.jpg">
                <h4>Официальная выездная церемония</h4>
                <ul>
                    <li>Подробные консультации и помощь в подготовке и сборе документов;</li>
                    <li>Доставка пары в выбранный муниципалитет в день подачи документов поиск свидетелей;</li>
                    <li>Проверка документов, перевод и оплата всех госпошлин;</li>
                    <li>Доставка пары на место церемонии;</li>
                    <li>Перевод церемонии;</li>
                    <li>Установка и украшение выбранной арки в тон и по стилю живыми цветами и текстилем;</li>
                    <li>Букет невесты и бутоньерка жениха;</li>
                    <li>Столик для церемонии, бутылка охлажденного шампанского и фужеры;</li>
                    <li>Фруктовый или сладкий фуршет (до 6 человек);</li>
                    <li>Минимальный декор места церемонии (вазы, подсвечники, полочки);</li>
                    <li>Лепестки для осыпания пары;</li>
                    <li>Сувенир на память;</li>
                    <li>Доставка апостилированного свидетельства в номер пары в течение 2 дней;</li>
                    <li>Услуги стилиста-визажиста;</li>
                    <li>Фотограф и видео (3 часа).</li>
                </ul>
                <p>1780 €</p>
            </li>
            <li class="Services-Item">
                <img class="Services-Image" src="/img/services/service-02.jpg">
                <h4>Символическая церемония - пакет "Полный"</h4>
                <ul>
                    <li>Помощь и консультации по всем вопросам подготовки церемонии;</li>
                    <li>Подготовка индивидуального сценария;</li>
                    <li>Доставка пары на место церемонии;</li>
                    <li>Установка выбранной арки и украшение искусственными цветами и текстилем;</li>
                    <li>Декорирование места церемонии по стилю или тематике церемонии;</li>
                    <li>Стулья для гостей (до 12 чел), столик для церемонии;</li>
                    <li>Украшение колечек;</li>
                    <li>Фруктовый фуршет и 3 бутылки охлажденного шампанского;</li>
                    <li>Капкейки в подарок;</li>
                    <li>Проведение песочной церемонии;</li>
                    <li>Лепестки для осыпания пары;</li>
                    <li>Сертификат о проведении символической церемонии;</li>
                    <li>Букет невесты и бутоньерка жениха;</li>
                    <li>Услуги стилиста-визажиста;</li>
                    <li>Фотограф и видео (3 часа).</li>
                </ul>
                <p>1250 €</p>
            </li>
            <li class="Services-Item">
                <img class="Services-Image" src="/img/services/service-03.jpg">
                <h4>Символическая церемония - пакет "Комфорт"</h4>
                <ul>
                    <li>Проведение символической церемонии по стандартному сценарию;</li>
                    <li>Оформление классической округлой арки искусственными цветами и тканью (в любом желаемом
                        цвете);
                    </li>
                    <li>Столик для церемонии;</li>
                    <li>Оформление колечек;</li>
                    <li>Бутылка шампанского и фужеры;</li>
                    <li>Сертификат о проведении символической церемонии;</li>
                    <li>Лепестки для осыпания пары.</li>
                </ul>
                <p>300 €</p>
            </li>
        </ul>
    </div>
</section>

<section class="Services Light" id="ceremonies">
    <div class="Services-Wrapper">
        <div class="Services-Header">
            <div class="Header-Spot-1"><span></span></div>
            <h3 class="Section-Header">Символические церемонии</h3>
            <div class="Header-Spot-2"><span></span></div>
        </div>

        <ul class="Services-List">
            <li class="Services-Item">
                <img class="Services-Image" src="/img/services/service-symb-03.jpg">
                <h4>Пакет "So happy!"</h4>
                <ul>
                    <li>Консультации по всем вопросам организации церемонии и пребывания на Кипре;</li>
                    <li>Проведение церемонии с индивидуально составленным текстом;</li>
                    <li>Доставка пары к месту церемонии;</li>
                    <li>Классическая округлая арка, украшенная наполовину искусственными цветами (или небольшим
                        количеством живых) и тканью (в любом выбранном цвете);
                    </li>
                    <li>Столик и декор места церемонии в тон или по теме;</li>
                    <li>Бутылка шампанского и фужеры;</li>
                    <li>Фруктовый или сладкий фуршет(на выбор);</li>
                    <li>Лепестки для осыпания;</li>
                    <li>Букет невесты и бутоньерка жениха(розы, герберы или лизиантусы);</li>
                    <li>Сертификат о проведении символической церемонии;
                    </li>
                    <li>Стулья для гостей до 6 человек.</li>
                </ul>
                <p>500 €</p>
            </li>
            <li class="Services-Item">
                <img class="Services-Image" src="/img/services/service-symb-04.jpg">
                <h4>Пакет "Счастливый случай"</h4>
                <ul>
                    <li>Консультации по всем возникшим вопросам;</li>
                    <li>Доставка пары на место церемонии;</li>
                    <li>Проведение церемонии по индивидуально составленному сценарию;</li>
                    <li>Классическая округлая арка по бокам или сбоку украшенная живыми цветами + тканью в любом
                        выбранном цвете;
                    </li>
                    <li>Столик и декор места церемонии в тон или по теме;</li>
                    <li>Фруктовый + сладкий фуршет, 4 бутылки шампанского, фужеры для пары и гостей;</li>
                    <li>Стулья для гостей (до 30 человек);</li>
                    <li>Проведение песочной церемонии;</li>
                    <li>Лепестки для осыпания;</li>
                    <li>Оформление колечек;</li>
                    <li>Букет невесты и бутоньерка жениха из сезонных цветов (в соответствии с цветами на арке).</li>
                </ul>
                <p>700 €</p>
            </li>
            <li class="Services-Item">
                <img class="Services-Image" src="/img/services/service-symb-05.jpg">
                <h4>Пакет на двоих "You, me and the sea"</h4>
                <ul>
                    <li>Консультативная помощь;</li>
                    <li>Доставка пары на место церемонии;</li>
                    <li>Проведение церемонии;</li>
                    <li>Классическая арка, украшенная тканью и морскими сувенирами;</li>
                    <li>Немного декора места церемонии в морском стиле;</li>
                    <li>Столик + мини-фуршет + 1 б шампанского + фужеры;</li>
                    <li>Проведение песочной церемонии;</li>
                    <li>Оформление колечек в морском стиле;</li>
                    <li>Лепестки для осыпания;</li>
                    <li>Букет невесты и бутоньерка жениха из роз и морских звезд, ракушек;</li>
                    <li>Сертификат о проведении церемонии.</li>
                </ul>
                <p>420 €</p>
            </li>
            <li class="Services-Item">
                <img class="Services-Image" src="/img/services/service-symb-02.jpg">
                <h4>Пакет на двоих "Бриз"</h4>
                <ul>
                    <li>Консультации по всем вопросам;</li>
                    <li>Доставка пары на место церемонии;</li>
                    <li>Проведение церемонии;</li>
                    <li>Украшение дерева текстилем и искусственными цветами(или небольшим букетом живых);</li>
                    <li>Столик + 1 бутылка шампанского + 2 фужера;</li>
                    <li>Немного декора места церемонии (подсвечники,клетки, вазы-на выбор);</li>
                    <li>Сертификат о проведении церемонии.</li>
                </ul>
                <p>250 €</p>
            </li>
        </ul>
    </div>
</section>