<section class="Reviews" id="reviews">

    <div class="Reviews-Header">
        <div class="Header-Spot-1"><span></span></div>
        <h3>Отзывы</h3>
        <div class="Header-Spot-2"><span></span></div>
    </div>


    <ul class='Reviews-List '>
        <li class="Reviews-List-Item">

            <div class="Reviews-Author">
                <div class="JS-Image-Align" data-image-ratio='1/1' data-image-position='center'>
                    <img src="/img/reviews/review-1.jpg" alt="">
                </div>
            </div>

            <article>
                <h4>Наташа</h4>
                <p>Я безумно счастлива и рада, что организацией нашей свадьбы занималось агентство "With love",
                   которое
                   полностью оправдывает своё название!</p>
                <p>Я очень долго выбирала тех, кому смогла бы доверить проведение свадьбы: смотрела фото уже
                   проведённых
                   церемоний, сравнивала букеты, арки, места проведения церемоний, цены...Ох, как вспомню :) Голова
                   была
                   в полном замешательстве, ведь от выбора команды зависело почти все.</p>
                <div class="Hidden">
                    <p>К счастью, я нашла для себя лучших организаторов свадеб на Кипре Ведущая церемоний и декоратор,
                       генератор всех идей — Альбина, организовала всё на высшем уровне, а самое главное — с душой и
                       любовью. Я сама рисовала и присылала эскизы своего видения арки, декораций и примерно знала, как
                       все
                       будет выглядеть, однако Альбина сделала не просто "что попросили", а придумала ещё множество
                       деталей,
                       нюансов, от которых моё настроение и радость в день свадьбы взлетели до небес. Только на самой
                       церемонии я поняла насколько же все очаровательно и до безумия прекрасно! Мы с мужем и гости были
                       в
                       полном восторге! Все прошло намного лучше, чем я хотела. Все благодаря Альбине.</p>
                    <p>Также хочу выразить просто огромную благодарность визажисту Инне! Она — настоящий профессионал и
                       тот
                       человек, который поднимает настроение невесте с самого утра! В тот день она была моей феей
                       Ещё огромнейшее спасибо фотографу Ильдару! Отличный фотограф, с которым легко работать, без
                       всякого
                       волнения. Он всегда подскажет как лучше позировать, с точностью до каждой детали: куда
                       посмотреть,
                       как встать, куда положить руку. Абсолютно не приходилось думать над каждым кадром и волноваться
                       за
                       снимки.</p>
                    <p>Я очень уважаю и ценю людей, которые работают не только ради денег, а ради удовольствия. Альбина,
                       Инна и Ильдар именно такие люди! Они вкладывают душу и делают столь важное событие, как свадьба,
                       незабываемым!</p>
                    <p>P.S. Я была настоящей Золушкой с самым прекрасным принцем и феей, которая попала в сказку!
                       Спасибо!</p>
                </div>
                <a class='More' href="#">Подробнее</a>
            </article>
        </li>

        <li class="Reviews-List-Item">
            <div class="Reviews-Author">
                <div class="JS-Image-Align" data-image-ratio='1/1' data-image-position='center'>
                    <img src="/img/reviews/review-2.jpg" alt="">
                </div>
            </div>

            <article>
                <h4>Алина Супрун</h4>
                <p>Хотим выразить огромное спасибо всей команде, которые сделали наш день просто сказочным.</p>
                <p>Спасибо за созданную красоту Альбине, все было как в самой настоящей сказка, вы как самая
                   настоящая фея.</p>
                <p>Спасибо Инночке за прекрасную работу над моими волосами, честно я думала, мы с ними ничего не
                   сможем сделать, ты чудо!!!</p>
                <p>Спасибо за нежнейший макияж, все было, так как я представляла.</p>
                <div class="Hidden">
                    <p>Спасибо нашему фотографу Ирине за такие чудесные фотографии, вы генератор идей и энергии.</p>
                    <p>Желаем всем процветания в вашем сказочном деле, творческих успехов и безумных идей.
                       Денис и Алина.</p>
                </div>
                <a class='More' href="#">Подробнее</a>
            </article>
        </li>

        <li class="Reviews-List-Item">
            <div class="Reviews-Author">
                <div class="JS-Image-Align" data-image-ratio='1/1' data-image-position='center'>
                    <img src="/img/reviews/review-3.jpg" alt="">
                </div>
            </div>

            <article>
                <h4>Ирина Бутцева</h4>
                <p>Не останусь в сторонке и я со своим отзывом!это даже больше был всплеск эмоций, который до сих
                   пор не проходит. Церемония была 2 года назад. Решение сделать свадьбу на Кипре была спонтанной,
                   т.к. нам не хотелось много народа, а хотелось чтобы мы были вдвоем:я, он и моооооре!Начались
                   поиски организаторов и везде пугали либо космические цены на пакеты церемонии в которых по сути и
                   ничего не было, либо предоплата 300 евро, а это простите не малые деньги, которые я должна была
                   отправить чужому человеку, либо организаторы не составляли договор, а без него было страшновато
                   ехать, т.к. "кидал" очень много... а общалась я наверно с 95% организаторами свадеб на Кипре.</p>
                <div class="Hidden">
                    <p>Первой кого я нашла это была фотограф, чудесная и замечательная, хохотушка и ооочень общительная
                       Ирина Акиньшина, вот она мне и рассказала все подробности фотосъемки, мы определили выгодное
                       время съемки, количество часов и Ирина рассказала какие локации мы посетим, сколько фото у нас
                       будет и естественно цену. Фото пообещала в течение 14 дней, меня это устраивало, т.к. у нас после
                       церемонии был отпуск и поэтому мы не спешили. Ирина, по моей просьбе посоветовала организатора
                       свадеб-Альбиночку! Ни на что хорошее я уже не надеялась, но ради интереса я зашла посмотреть
                       наполнение пакетов и сравнить их и цены на них. Сначала я просто не поверила ценам, подумала что
                       давно не обновляли, но написала и попросила подробности. Альбина мне ответила сразу же,
                       досконально все разъяснила, оказывается цены актуальны.</p>

                    <p>Был вопрос с пляжем, где проводить церемонию, т.к. я хотела максимально светлый песок, а это
                       только в Напе, а там за проведение свадеб муниципалитет стал брать налог. Альбина из кожи вон
                       вылезла, но нашла место с белым песком
                       и бесплатно!!! Альбина тоже как оказалось не берет предоплату, что меня безоговорочно устраивало.
                       Самое волнительное было то, когда мы прилетели, было страшно звонить и не дозвониться...услышать
                       что мы вас не знаем и прочее...Церемония была у нас на 3 день после прилета и все эти дни мы были
                       постоянно на связи, обсуждали как нас заберут, отвезут, во сколько, что и как...В НАШ самый
                       счастливый день Альбина приехала за нами, хотя до этого предупреждала что возможно ее не будет
                       при проведении церемонии... я насторожилось, оказалось зря!</p>

                    <p>Мы приехав на место церемонии увидели Инну)именно она тогда проводила нашу церемонию, т.к.
                       Альбиночка в свой последний беременный денек
                       все организовала по высшему разряду и со спокойной душой уехала рожать! Я восхищаюсь ей! У нас
                       оказались 2 услуги дополнительные, не заявленные в пакете, мы были очень довольны! Я с
                       удовольствием, не ради рекламы искренне советую всем своим подружкам и знакомым, а также не
                       знакомым на девичьих форумах девочкам этот чудесный, искренний коллектив! Я сама по себе ооочень
                       придирчивый человек, но мне реально не к чему было придраться! Коллектив Альбины по праву должен
                       занимать первые строчки в рейтинге организаторов свадеб на Кипре! Девочки процветания Вам,
                       бесконечных идей, и нескончаемого позитива, который Вы дарите НАМ!!!</p>
                </div>
                <a class='More' href="#">Подробнее</a>
            </article>
        </li>

        <li class="Reviews-List-Item">
            <div class="Reviews-Author">
                <div class="JS-Image-Align" data-image-ratio='1/1' data-image-position='center'>
                    <img src="/img/reviews/review-4.jpg" alt="">
                </div>
            </div>

            <article>
                <h4>Марина Растикхина</h4>
                <p>Хотелось бы оставить не отзыв, а просто сказать ОГРОМНОЕ СПАСИБО за проделанную работу, за то
                   волшебство, которое сотворила команда агентства "With Love" в день нашей свадьбы!</p>
                <p>При подготовке к свадьбе не возникло недопонимания, наоборот, впечатлило то, что Альбиночка -
                   настоящий мастер своего дела: она помогла разобраться со всеми нюансами, сориентировать по
                   оформлению, у нее никогда не заканчиваются идеи! Порадовало то, что с Альбиной очень приятно
                   общаться, чувствуешь, как человек полностью проникает во всю эту атмосферу и вкладывает все свои
                   силы! Нам безумно повезло встретить человека, близкого нам по духу!</p>
                <div class="Hidden">
                    <p>Сама церемония превзошла все ожидания и была просто на высшем уровне!!! Пожалуй, не устану
                       повторять: Альбиночка просто волшебница!!! Свадебная фея! Спасибо тебе за то, что ты смогла так
                       точно и непринужденно передать то настроение, которое царило в день нашей свадьбы! С тобой было
                       очень легко и приятно работать!</p>
                    <p>От всей души желаем процветания агентству "With Love", Вы в нашем сердце навсегда!
                       Женя и Марина</p>
                </div>
                <a class='More' href="#">Подробнее</a>
            </article>
        </li>

        <li class="Reviews-List-Item">
            <div class="Reviews-Author">
                <div class="JS-Image-Align" data-image-ratio='1/1' data-image-position='center'>
                    <img src="/img/reviews/review-5.jpg" alt="">
                </div>
            </div>

            <article>
                <h4>Катерина Лазарева</h4>
                <p>Дорогие влюблённые!!! Если Вы на этой страничке, значит совсем скоро настанет тот день, когда
                   Ваши сердца соединятся и Вы увековечите этот особенный момент самым красивым образом))) я хотела
                   бы выразить нашу искреннюю благодарность агенству "With Love" и лично нашему организаторам
                   Альбине и её мужу Николасу!</p>
                <p>Этот день был безупречным, благодаря любящему тандему. Мы получили
                   огромное удовольствие при подготовке свадебной церемонии у моря и венчания в церкви Святого
                   Илиаса.</p>
                <div class="Hidden">
                    <p>Были учтены совершенно все наши пожелания и даже больше...Вспоминая каждый момент нашего
                       свадебного дня, у меня захватывает дух и я смело могу признаться, что это был самый лучший день в
                       нашей жизни!!! Любите друг друга, женитесь и обязательно венчайтесь на Кипре!!! После таинств,
                       Вам обеспечено благословение на рождение здоровых, красивых детей и процветание!!!</p>
                </div>
                <a class='More' href="#">Подробнее</a>
            </article>
        </li>

        <li class="Reviews-List-Item">
            <div class="Reviews-Author">
                <div class="JS-Image-Align" data-image-ratio='1/1' data-image-position='center'>
                    <img src="/img/reviews/review-6.jpg" alt="">
                </div>
            </div>

            <article>
                <h4>Оксана Мигно</h4>
                <p>Добрый день всем!</p>
                <p>Хочу так же присоединиться ко всем восторженным отзывам. Мы были гостями на
                   одной из свадебных церемоний, организованных агентством "With Love". Сказать что это было
                   потрясающе, значит не сказать ничего. Нам очень понравилось. И в какой то момент я даже пожалела,
                   что моя личная свадьба проходила в холодной и зимней Москве.</p>
                <div class="Hidden">
                    <p>Насколько необычно, красиво и сказочно проходит эта церемония на острове, который только и
                       предназначен для любви, ведь
                       островом Кипр руководит Венера, а она у нас отвечает за любовь. На мой взгляд это самое лучшее
                       место где нужно начать свой совместный путь вдвоем, чтобы заложить прочный фундамент дальнейших
                       взаимоотношений построенных на любви верности и уважении.</p>
                    <p>Большое спасибо Альбине, Николасу и их
                       очаровательным деткам, за то, что они оставили довольно яркие и незабываемые впечатления.</p>
                </div>
                <a class='More' href="#">Подробнее</a>
            </article>
        </li>

        <li class="Reviews-List-Item">
            <div class="Reviews-Author">
                <div class="JS-Image-Align" data-image-ratio='1/1' data-image-position='center'>
                    <img src="/img/reviews/review-8.jpg" alt="">
                </div>
            </div>

            <article>
                <h4>Эля Патурина</h4>
                <p>Вот уже прошёл год, как самый лучший мужчина на свете сделал мне предложение 😜 Традиционную
                   свадьбу делать мы не хотели, хотелось в первую очередь сделать приятный, нехлопотный, особенный
                   праздник для себя. И мы решили сделать красивую свадебную церемонию где-нибудь заграницей.
                   Наш выбор пал на остров любви Кипр. До этого мы там уже бывали и просто влюбились в этот
                   чудо-остров.</p>
                <div class="Hidden">
                    <p>И вот я начала бороздить просторы Интернета в поисках свадебного агенства. Просмотрела множество
                       сайтов, страничек в соцсетях и нашла прекрасного человека, нашу Альбиночку!
                       Альбина на протяжении полугода, что мы с ней общались, слушала мои хотелки, которые менялись с
                       определённой переодичностью подбадривала меня, успокаивала и делилась со мной своими идеями.
                       Альбина предложила нам просто сумасшедшую площадку для проведения церемонии, на которую до нас
                       почему-то никто не соглашался))) Бескрайнее море сказочного цвета, прибрежные скалы, ласковое
                       солнце и живописный обрыв, на котором стояли мы, счастливые и окрылённые. Абсолютно безлюдное
                       место, слышно только шум морских волн, бьющихся о скалы, и крики чаек. И какая-то энергетика там
                       была, особая. Когда мы подъезжали к площадке и я увидела всю красоту, созданную Альбиной, у меня
                       полились слезы счастья. Насколько точно Альбина понимала меня, всё, что я хочу... это было просто
                       потрясающе!</p>
                    <p>Красивейшая арка, дорожка, фуршетный столик, гостевые стульчики, декор, букет... всё было в
                       едином
                       стиле. Всё было продумано до мелочей, хотелось разглядывать и разглядывать всю эту красоту.
                       Выбранные нами цвета просто идеально сочетались с красками моря и солнца.
                       Альбиночка провела самую красивую и трогательную церемонию, которую только можно представить. Это
                       было настолько искренне и душевно, что хочется вспоминать и вспоминать эти моменты.
                       Это говорит только о том, что ты занимаешься именно своим делом и вкладываешь в него всю свою
                       душу.</p>
                    <p>Мы хотим сказать огромное спасибо за проделанную работу, за сказку, которую ты создала! Спасибо
                       за то, что была рядом со мной на протяжении полугода подготовки, спасибо за поддержку, за советы,
                       просто за девчачью болтовню. Ты не просто сумасшедший свадебный организатор, но и очень
                       хороший, добрый, светлый, порядочный друг и человек.
                       То, что ты сделала — бесценно, ведь ты подарила нам счастье в столь волнующий и очень важный для
                       нас день.</p>
                </div>
                <a class='More' href="#">Подробнее</a>
            </article>
        </li>

        <li class="Reviews-List-Item">
            <div class="Reviews-Author">
                <div class="JS-Image-Align" data-image-ratio='1/1' data-image-position='center'>
                    <img src="/img/reviews/review-7.jpg" alt="">
                </div>
            </div>

            <article>
                <h4>Валерия Суручкина</h4>
                <p>Здравствуйте! Хочу рассказать как я вышла за муж два года назад:) нагружать отзыв, почему мы
                   выбрали Кипр не стану, да и на Альбину я вышла случайно. Опыта организаций подобного у меня было
                   ноль! Поэтому я полностью доверилась Альбине, вот только цвет арки выбрала - фиолетовый, мой
                   любимый. Альбина предложила, раз цвет фиолетовый, стиль прованс. Я, как человек с сугубо
                   техническим образованием, о провансе представление особо не имела. Поэтому смело согласилась.</p>
                <div class="Hidden">
                    <p>Больше, кроме платья, я особо ничем не заморачивалась. Волнение пришло уже на острове, за пару
                       дней до церемонии. Выездные свадьбы я видела в России. Украшенные цветной бумагой деревья,
                       ломящиеся от колбасы и алкоголя шатающиеся столы и всё в этом духе... О боже мой, что же для меня
                       приготовил совершенно незнакомый человек?!</p>
                    <p>Настал день икс. Когда мы приехали с фотосессии на место церемонии я не поверила глазам.... Я
                       никогда не думала, что может быть так красиво... Живые цветы, множество изящных аксессуаров,
                       столик с фруктами и шампанским.... Всё это невероятно сочеталось и гармонировало друг с другом. К
                       сожалению, я не владею настолько пером, чтобы всё это передать словами. Но поэт бы тут
                       использовал самые сказочно красивые эпитеты. А ещё там был диван! Винтажный фиолетовый велюровый
                       диван! На пляже! Альбина и ее команда затмили своей работой даже красоту Кипра.</p>
                    <p>А фотограф Ирина запечатлела мою свадьбу на фотографиях. Я с гордостью показываю свой фотоальбом
                       всем знакомым!</p>
                    <p>Дорогая Альбиночка, спасибо вам огромное за то, что вы столько сделали для меня.Вы - фея,
                       волшебница.</p>
                </div>
                <a class='More' href="#">Подробнее</a>
            </article>
        </li>
    </ul>
    </div>
</section>