<section class="Places" id="places">
    <div class="Places-Header">
        <div class="Header-Spot-1"><span></span></div>
        <h3>Места свадеб</h3>
        <div class="Header-Spot-2"><span></span></div>
    </div>

    <div class="Places-Slider JS-Carousel">
        <ul class='Places-Slider-List JS-Carousel-List'>
            <li class="Places-Slider-Item JS-Carousel-Item">
                <div style="background-image:url('/img/places/place-01.jpg');" alt=""></div>
                <article>
                    <h4>Пляж Потамо</h4>
                    <p>Белый песочек. В 10 мин езды от Айя-Напы. (символические церемонии)</p>
                </article>
            </li>

            <li class="Places-Slider-Item JS-Carousel-Item">
                <div style="background-image:url('/img/places/place-02.jpg');" alt=""></div>
                <article>
                    <h4>Пляж Каппарис</h4>
                    <p>Место находится в 15 мин от Протараса. Нижняя площадка. (официальные и символические
                       церемонии)</p>
                </article>
            </li>

            <li class="Places-Slider-Item JS-Carousel-Item">
                <div style="background-image:url('/img/places/place-03.jpg');" alt=""></div>
                <article>
                    <h4>Пляж Каппарис</h4>
                    <p>Место находится в 15 мин от Протараса. Верхняя площадка. (официальные и символические
                       церемонии)</p>
                </article>
            </li>


            <li class="Places-Slider-Item JS-Carousel-Item">
                <div style="background-image:url('/img/places/place-04.jpg');" alt=""></div>
                <article>
                    <h4>Белые скалы Аламана</h4>
                    <p>Неподалеку от Лимассола. (символические церемонии)</p>
                </article>
            </li>

            <li class="Places-Slider-Item JS-Carousel-Item">
                <div style="background-image:url('/img/places/place-05.jpg');" alt=""></div>
                <article>
                    <h4>Белые скалы за Пафосом</h4>
                    <p>Верхняя площадка. (символические и официальные церемонии)</p>
                </article>
            </li>

            <li class="Places-Slider-Item JS-Carousel-Item">
                <div style="background-image:url('/img/places/place-06.jpg');" alt=""></div>
                <article>
                    <h4>Белые скалы за Пафосом</h4>
                    <p>Нижняя площадка 1. (символические и официальные церемонии)</p>
                </article>
            </li>

            <li class="Places-Slider-Item JS-Carousel-Item">
                <div style="background-image:url('/img/places/place-07.jpg');" alt=""></div>
                <article>
                    <h4>Белые скалы за Пафосом</h4>
                    <p>Нижняя площадка 2. (символические и официальные церемонии)</p>
                </article>
            </li>
        </ul>
    </div>
</section>