<section class="Header">
    <ul class="Header-Menu JS-Page-Navigation" data-scroll-shift="48">
        <li><a href="#services">Услуги</a></li>
        <li><a href="#kipr">Почему Кипр</a></li>
        <li><a href="#gallery">Галерея</a></li>
        <li><a href="#video">Видео</a></li>
        <li><a href="#about">О нас</a></li>
        <li><a href="#reviews">Отзывы</a></li>
        <li><a href="#contacts">Контакты</a></li>
    </ul>

    <div class='Header-Wrapper'>
        <div class="Header-Logo">
            <span>А</span><img src='/img/ico/logo.png'><span>А</span>
        </div>

        <div class="Header-With-Love">
            <img src="/img/ico/with-love.png" alt="With Love">
        </div>

        <div class="Header-Call"><span>Свяжитесь с нами</span></div>
        <div class="Header-Slider JS-Carousel">
            <ul class='Header-Slider-List JS-Carousel-List'>
                <li class="Header-Slider-Item JS-Carousel-Item">
                    <div style="background-image:url('/img/slider/slide-01.jpg');" alt=""></div>
                </li>
                <li class="Header-Slider-Item JS-Carousel-Item">
                    <div style="background-image:url('/img/slider/slide-02.jpg');" alt=""></div>
                </li>
                <li class="Header-Slider-Item JS-Carousel-Item">
                    <div style="background-image:url('/img/slider/slide-03.jpg');" alt=""></div>
                </li>
                <li class="Header-Slider-Item JS-Carousel-Item">
                    <div style="background-image:url('/img/slider/slide-04.jpg');" alt=""></div>
                </li>
            </ul>
        </div>
        <div class="Header-Description">
            <h3>Свадьба на Кипре</h3>
            <p>Не упустите возможность запомнить лучшим один из главных дней!</p>
        </div>
    </div>
</section>