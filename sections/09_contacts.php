<section class="Contacts" id="contacts">

    <div class="Contacts-Header">
        <div class="Header-Spot-1"><span></span></div>
        <h3>Контакты</h3>
        <div class="Header-Spot-2"><span></span></div>
    </div>

    <div class="Contacts-Wrapper">
        <div class="Contacts-Map" id="map"></div>


        <div class="Contacts-Info">
            <dl>
                <dt>Контактное лицо</dt>
                <dd>Альбина</dd>
            </dl>
            <dl>
                <dt>Телефон</dt>
                <dd>+35 79 905 30 54<br>(Viber, WhatsApp, WeChat)</dd>
            </dl>
            <dl>
                <dt>Skype</dt>
                <dd>bliznezi33</dd>
            </dl>
            <dl>
                <dt>Почтовый адрес</dt>
                <dd>bina.vasilas@mail.ru</dd>
            </dl>
        </div>
    </div>


    <div class="Contacts-Docs-Header" id="info">
        <div class="Header-Spot-1"><span></span></div>
        <h3>Информация</h3>
        <div class="Header-Spot-2"><span></span></div>
    </div>

    <div class="Contacts-Docs-List">
        <div class="Contacts-Docs-Item">
            <h3>Документы необходимые для официальной регистрации брака на Кипре:</h3>
            <ul>
                <li>Свидетельство о рождении, переведенное на английский язык;</li>
                <li>Справка о семейном положении, также с переводом на английский язык и заверенная у нотариуса;</li>
                <li>Загранпаспорт;</li>
                <li>Если ранее состояли в браке, то понадобится свидетельство о разводе с переводом на английский язык,
                    заверенное у нотариуса.
                </li>
            </ul>
        </div>
        <div class="Contacts-Docs-Item">
            <h3>Документы необходимые для венчания на Кипре:</h3>
            <ul>
                <li>Копия свидетельства о рождении, переведенная на английский язык и заверенная у нотариуса;</li>
                <li>Свидетельство о крещении, переведенное на английский язык и заверенное у нотариуса;</li>
                <li>Свидетельство об уже заключенном общем браке с переводом на английский язык, заверенное у нотариуса;</li>
                <li>Загранпаспорт.</li>
            </ul>
        </div>
    </div>

</section>
