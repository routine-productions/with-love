<section class="Gallery" id="gallery">
    <div class="Gallery-Wrapper">
        <div class="Gallery-Header">
            <div class="Header-Spot-1"><span></span></div>
            <h3>Наши свадьбы</h3>
            <div class="Header-Spot-2"><span></span></div>
            <p>Незабываемые моменты, застывшие в фотографиях</p>
        </div>

        <div class="Gallery-List">
            <div class="Gallery-Line">
                <div class="Gallery-Vertical">
                    <div class="JS-Image-Align" data-image-ratio='1/1' data-image-position='center'>
                        <img src="/img/gallery/gallery-01.jpg" alt=""></div>
                    <div class="JS-Image-Align" data-image-ratio='1/1' data-image-position='center'>
                        <img src="/img/gallery/gallery-02.jpg" alt=""></div>
                </div>
                <div class="Gallery-Big">
                    <div class="JS-Image-Align" data-image-ratio='1/1' data-image-position='center'>
                        <img src="/img/gallery/gallery-03.jpg" alt=""></div>
                </div>
                <div class="Gallery-Vertical">
                    <div class="JS-Image-Align" data-image-ratio='1/1' data-image-position='center'>
                        <img src="/img/gallery/gallery-04.jpg" alt=""></div>
                    <div class="JS-Image-Align" data-image-ratio='1/1' data-image-position='center'>
                        <img src="/img/gallery/gallery-05.jpg" alt=""></div>
                </div>
                <div class="Gallery-Big">
                    <div class="JS-Image-Align" data-image-ratio='1/1' data-image-position='center'>
                        <img src="/img/gallery/gallery-06.jpg" alt=""></div>
                </div>
                <div class="Gallery-Vertical">
                    <div class="JS-Image-Align" data-image-ratio='1/1' data-image-position='center'>
                        <img src="/img/gallery/gallery-07.jpg" alt=""></div>
                    <div class="JS-Image-Align" data-image-ratio='1/1' data-image-position='center'>
                        <img src="/img/gallery/gallery-08.jpg" alt=""></div>
                </div>
            </div>

            <div class="Gallery-Line">
                <div class="Gallery-Big">
                    <div class="JS-Image-Align" data-image-ratio='1/1' data-image-position='center'>
                        <img src="/img/gallery/gallery-09.jpg" alt=""></div>
                </div>


                <div class="Gallery-Vertical">
                    <div class="JS-Image-Align" data-image-ratio='1/1' data-image-position='center'>
                        <img src="/img/gallery/gallery-10.jpg" alt=""></div>
                    <div class="JS-Image-Align" data-image-ratio='1/1' data-image-position='center'>
                        <img src="/img/gallery/gallery-11.jpg" alt=""></div>
                </div>


                <div class="Gallery-Vertical">
                    <div class="JS-Image-Align" data-image-ratio='1/1' data-image-position='center'>
                        <img src="/img/gallery/gallery-12.jpg" alt=""></div>
                    <div class="JS-Image-Align" data-image-ratio='1/1' data-image-position='center'>
                        <img src="/img/gallery/gallery-13.jpg" alt=""></div>
                </div>


                <div class="Gallery-Vertical">
                    <div class="JS-Image-Align" data-image-ratio='1/1' data-image-position='center'>
                        <img src="/img/gallery/gallery-14.jpg" alt=""></div>
                    <div class="JS-Image-Align" data-image-ratio='1/1' data-image-position='center'>
                        <img src="/img/gallery/gallery-15.jpg" alt=""></div>
                </div>
                <div class="Gallery-Big">
                    <div class="JS-Image-Align" data-image-ratio='1/1' data-image-position='center'>
                        <img src="/img/gallery/gallery-16.jpg" alt=""></div>
                </div>
            </div>

            <div class="Gallery-Line">
                <div class="Gallery-Vertical">
                    <div class="JS-Image-Align" data-image-ratio='1/1' data-image-position='center'>
                        <img src="/img/gallery/gallery-17.jpg" alt=""></div>
                    <div class="JS-Image-Align" data-image-ratio='1/1' data-image-position='center'>
                        <img src="/img/gallery/gallery-18.jpg" alt=""></div>
                </div>
                <div class="Gallery-Big">
                    <div class="JS-Image-Align" data-image-ratio='1/1' data-image-position='center'>
                        <img src="/img/gallery/gallery-19.jpg" alt=""></div>
                </div>
                <div class="Gallery-Vertical">
                    <div class="JS-Image-Align" data-image-ratio='1/1' data-image-position='center'>
                        <img src="/img/gallery/gallery-20.jpg" alt=""></div>
                    <div class="JS-Image-Align" data-image-ratio='1/1' data-image-position='center'>
                        <img src="/img/gallery/gallery-21.jpg" alt=""></div>
                </div>
                <div class="Gallery-Big">
                    <div class="JS-Image-Align" data-image-ratio='1/1' data-image-position='center'>
                        <img src="/img/gallery/gallery-22.jpg" alt=""></div>
                </div>
                <div class="Gallery-Vertical">
                    <div class="JS-Image-Align" data-image-ratio='1/1' data-image-position='center'>
                        <img src="/img/gallery/gallery-23.jpg" alt=""></div>
                    <div class="JS-Image-Align" data-image-ratio='1/1' data-image-position='center'>
                        <img src="/img/gallery/gallery-24.jpg" alt=""></div>
                </div>
            </div>
        </div>
    </div>
</section>