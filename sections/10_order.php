<section class="Order">
    <div class="Order-Header">
        <div class="Header-Spot-1"><span></span></div>
        <h3>Заказать церемонию</h3>
        <div class="Header-Spot-2"><span></span></div>
    </div>

    <div class="Order-Wrapper">
        <form class='JS-Form'
              data-form-email='bina.vasilas@mail.ru'
              data-form-subject='With Love: Перезвоните мне'
              data-form-url='/js/form.php'
              data-form-method='POST'>
            <p>Оставьте нам свой телефон и мы перезвоним:</p>
            <input type="text" placeholder="ваш телефон" name='name' data-input-title='Телефон'>
            <button class='JS-Form-Button'>Перезвоните мне</button>
            <div class="JS-Form-Result"></div>
        </form>
    </div>
</section>